extends Node2D


func _ready():
	var player_hp = $Player/Health
	var hp_bar = $HealthBar
	
	player_hp.connect("changed", hp_bar, "set_value")
	player_hp.connect("max_changed", hp_bar, "set_max")
	player_hp.initialize()
