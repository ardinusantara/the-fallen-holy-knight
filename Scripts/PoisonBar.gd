extends Node2D

signal max_changed(new_max)
signal changed(new_amount)
signal depleted

export(int) var max_amount = 10 setget set_max
onready var current = 0 setget set_current

func _ready():
	initialize()

func set_max(new_max):
	max_amount = new_max
	max_amount = max(1, max_amount)
	emit_signal("max_changed", max_amount)

func set_current(new_value):
	current = new_value
	current = clamp(current, 0, max_amount)
	emit_signal("changed", current)
	
func initialize():
	emit_signal("max_changed", max_amount)
	emit_signal("changed", current)
