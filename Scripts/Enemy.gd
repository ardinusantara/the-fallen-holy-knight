extends KinematicBody2D

const UP = Vector2(0,-1)
var SPEED = 28
var GRAVITY = 1000
var velocity = Vector2()
var isDead = false
var isDestroyed = false
var isMovingRight = true

func _ready() -> void:
	change_sprite($Walk)
	$AnimationPlayer.play("Walk")

func _physics_process(delta):
	if !$AnimationPlayer.current_animation == "Attack":
		if !isDestroyed:
			move_enemy(delta)
			isdead()

func move_enemy(delta):
	if !isDead:
		velocity.x = SPEED if isMovingRight else -SPEED
		velocity.y += delta * GRAVITY
		velocity = move_and_slide(velocity, Vector2.UP)
		if (!$RayCast2D.is_colliding() and is_on_floor()) or is_on_wall():
			isMovingRight = !isMovingRight
			scale.x = -scale.x

func isdead():
	if isDead:
		if !$AnimationPlayer.is_playing():
			isDestroyed = true
			queue_free()

func hurt():
	isDead = true
	change_sprite($Death)
	$AnimationPlayer.play("Death")
	self.get_child(0).queue_free()
	self.get_child(1).queue_free()
	self.get_child(2).queue_free()
	

func hit():
	$AttackDetector.monitoring = true
	if !$AttackSFX.playing:
		$AttackSFX.play(0)

func end_of_hit():
	$AttackDetector.monitoring = false

func start_walk():
	change_sprite($Walk)
	$AnimationPlayer.play("Walk")
	if $AttackSFX.playing:
		$AttackSFX.stop()
	
func _on_PlayerDetector_body_entered(body):
	change_sprite($Attack)
	$AnimationPlayer.play("Attack")

func _on_AttackDetector_body_entered(body):
	if body.get_name() == "Player":
		if self.position.x > body.position.x:
			body.flip_sprite(false) 
		elif self.position.x < body.position.x:
			body.flip_sprite(true)
		body.hit()

func change_sprite(exception: Node) -> void:
	for child in get_children():
		if child.get_class() == "Sprite" and child != exception:
			child.hide()
	exception.show()

func flip_sprite(direction: bool) -> void:
	for child in get_children():
		if child.get_class() == "Sprite":
			child.flip_h = direction
