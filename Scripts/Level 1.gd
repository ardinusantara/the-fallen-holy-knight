extends Node2D

func _ready():
	Global.show_portal = false
	$Player/TextureRect2.visible = false
	$Portal.visible = false
	$Portal/Area2D/CollisionShape2D.disabled = true

func _process(delta):
	if Global.show_portal:
		$Portal.visible = true
		$Portal/Area2D/CollisionShape2D.disabled = false
		
func _on_Area2D_body_entered(body):
	if body.get_name() == "Player":
		get_tree().change_scene(str("res://Scenes/Lobby 2.tscn"))


func _on_FailArea_body_entered(body):
	if body.get_name() == "Player":
		get_tree().change_scene(str("res://Scenes/GameOver.tscn"))
