extends Label

onready var timer = 30
onready var message = "Ready"

func _ready():
	self.text = message

func _process(delta):
	if get_parent().get_parent().isFullHealCdStart:
		timer -= delta
		if timer < 0:
			timer = 30
			message = "Ready"
		else:
			message = fmod(timer, 60)
			message = "%02d sec" % [message]
		self.text = str(message)
