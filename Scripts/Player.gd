extends KinematicBody2D

const UP = Vector2(0,-1)
var SPEED = 150
var GRAVITY = 1200
var JUMP_HEIGHT = -300
var heal_cooldown = 15
var fullheal_cooldown = 30
var heal_duration = 5

onready var health = $Health
onready var poison = $Health/Poison
onready var healcooldown_timer = $HealCoolDown
onready var healduration_timer = $HealDuration
onready var fullhealcooldown_timer = $FullHealCoolDown

var velocity = Vector2()
var heal_validator = false
var isRunning = false
var isAttacking = false
var isJumping = false
var isFalling = false
var isDoubleJump = false
var isHurt = false
var isDead = false
var isHealing = false
var isFullHealing = false
var isGameOver = false
var isHealCdStart = false
var isFullHealCdStart = false

func _ready():
	change_sprite($Idle)
	$AnimationPlayer.play("Idle")
	$BGM.play()
	initialize_health()
	initialize_poison()

func initialize_health():
	var player_hp = $Health
	var hp_bar = $Health/HealthBar
	
	player_hp.connect("changed", hp_bar, "set_value")
	player_hp.connect("max_changed", hp_bar, "set_max")
	player_hp.initialize()

func initialize_poison():
	var player_poison = $Health/Poison
	var poison_bar = $Health/PoisonBar
	
	player_poison.connect("changed", poison_bar, "set_value")
	player_poison.connect("max_changed", poison_bar, "set_max")
	player_poison.initialize()
	
func _physics_process(delta):
	if !isGameOver:
		isdead()
		
	is_gameover()
		
	if !isDead:
		get_input()
		velocity.y += delta * GRAVITY
		velocity = move_and_slide(velocity, UP)

func get_input():
	velocity.x = 0
	check_idle()
	
	if !heal_validator:
		run_poison()
	
	if !isJumping and !isFalling and !isHealing and !isFullHealing:
		check_attack()
		
	check_double_jump()
	
	if !isAttacking:
		check_heal()
		check_full_heal()
		if !isHealing and !isFullHealing and !isHurt:		
			check_jump()
			check_fall()
			check_run()

func check_idle():
	if !isRunning and !isAttacking and !isJumping and !isFalling and !isHealing and !isFullHealing and !isHurt:
		change_sprite($Idle)
		$AnimationPlayer.play("Idle")

func run_poison():
	poison.current += 0.01
	if poison.current == poison.max_amount:
		health.current -= 0.01

func check_attack():
	if Input.is_action_just_pressed("Attack"):
		change_sprite($Attack)
		$AnimationPlayer.play("Attack")
		if !$AttackSFX.playing:
			$AttackSFX.play(0)
		isAttacking = true
		if !$Attack.flip_h:
			$AttackArea/AttackCollision.disabled = false
		elif $Attack.flip_h:
			$AttackArea/AttackCollision2.disabled = false
	if !$AnimationPlayer.is_playing():
		isAttacking = false
		$AttackSFX.stop()
		if !$Attack.flip_h:
			$AttackArea/AttackCollision.disabled = true
		elif $Attack.flip_h:
			$AttackArea/AttackCollision2.disabled = true

func check_heal():
	if Input.is_action_just_pressed("Heal") and healcooldown_timer.is_stopped():
		if !isJumping and !isFalling and !isFullHealing:
			heal_validator = true
			isHealing = true
			change_sprite($Heal)
			$AnimationPlayer.play("Heal")
			healcooldown_timer.start(heal_cooldown)
			isHealCdStart = true
			healduration_timer.start(heal_duration)
			
	if healcooldown_timer.is_stopped():
		isHealCdStart = false
	
	if !$AnimationPlayer.is_playing():
		isHealing = false
	
	if heal_validator == true:
		health.current += 0.02
		poison.current -= 0.02
	
	if healduration_timer.is_stopped() or (health.current == health.max_amount and poison.current == 0):
		heal_validator = false

func check_full_heal():
	if Input.is_action_just_pressed("FullHeal") and fullhealcooldown_timer.is_stopped() and Global.isLevel2:
		if !isJumping and !isFalling and !isHealing:
			isFullHealing = true
			change_sprite($FullHeal)
			$AnimationPlayer.play("FullHeal")
				
			fullhealcooldown_timer.start(fullheal_cooldown)
			isFullHealCdStart = true
	
	if fullhealcooldown_timer.is_stopped():
		isFullHealCdStart = false	
		
	if isFullHealing and !$AnimationPlayer.is_playing():
		health.current = health.max_amount
		poison.current = 0
		
		isFullHealing = false

func check_jump():
	if !isJumping and Input.is_action_just_pressed("Jump"):
		velocity.y = JUMP_HEIGHT
		isJumping = true
		
		change_sprite($Jump)
		$AnimationPlayer.play("Jump")
	if isJumping and velocity.y == 0:
		isJumping = false

func check_double_jump():
	if !isDoubleJump and isJumping and Input.is_action_just_pressed("Jump"):
		velocity.y += JUMP_HEIGHT
		isDoubleJump = true
		change_sprite($Jump)
		$AnimationPlayer.play("Jump")
		
	if isDoubleJump and velocity.y == 0:
		isDoubleJump = false
		
func check_fall():
	if velocity.y > 200:
		isFalling = true
		
		change_sprite($Fall)
		$AnimationPlayer.play("Fall")
	if velocity.y == 0:
		isFalling = false

func check_run():
	if Input.is_action_pressed("ui_right") or Input.is_action_pressed("ui_left"):
		isRunning = true
		if !isJumping and !isFalling:
			change_sprite($Run)
			$AnimationPlayer.play("Run")
		if Input.is_action_pressed("ui_right"):
			flip_sprite(false)
			velocity.x += SPEED
		
		elif Input.is_action_pressed("ui_left"):
			flip_sprite(true)
			velocity.x -= SPEED
	
	elif Input.is_action_just_released("ui_right") or Input.is_action_just_released("ui_left"):
		isRunning = false

func is_gameover():
	if isGameOver and !$AnimationPlayer.is_playing():
		get_tree().change_scene(str("res://Scenes/GameOver.tscn"))
		
func isdead():
	if isHurt:
		var knockback = Vector2()
		if $Hurt.flip_h:
			knockback.x = 100
		else:
			knockback.x = -100

		knockback = move_and_slide(knockback, UP)
		if !$AnimationPlayer.is_playing():
			isHurt = false
		knockback.x = 0
	if health.current <= 0:
		isDead = true
	if isDead:
		if !$AnimationPlayer.is_playing():
			isGameOver = true
			change_sprite($Death)
			$AnimationPlayer.play("Death")

func hit():
	if !isDead and !isHurt and !isFullHealing:
		isHurt = true
		health.current -= 5
		change_sprite($Hurt)
		$AnimationPlayer.play("Hurt")

func _on_AttackArea_body_entered(body):
	if "Enemy" in body.get_name():
		body.hurt()
		
func change_sprite(exception: Node) -> void:
	for child in get_children():
		if child.get_class() == "Sprite" and child != exception:
			child.hide()
	exception.show()

func flip_sprite(direction: bool) -> void:
	for child in get_children():
		if child.get_class() == "Sprite":
			child.flip_h = direction

